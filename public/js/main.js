$(function () {
    $.getJSON('/api/v1/users', function (data) {
        $(data).each(function (i, v) {
            $('#username').append(
                $('<option></option>').attr('value', v).text(v)
            );
        });
    });

    $('#btnSend').click(function () {
        var fromUser = $('#fromUser').val();
        var username = $('#username option:selected').attr('value');
        var content = $('#content').val();

        if (username && content) {
            $.ajax({
                method: 'POST',
                url: `/api/v1/users/${username}/messages`,
                contentType: 'application/json',
                data: JSON.stringify({ 'fromUser': fromUser, 'content': content }),

                error: function (xhr, status, err) {
                    alert("Send Error: " + err);
                }
            });
        }
    });
});