var express = require('express')
    , router = express.Router();

var api = require('./api')

router.get('/hello', function (req, res) {
    res.send('Hello World!!!');
});

router.use('/api/v1', api);

module.exports = router;