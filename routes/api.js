var express = require('express')
    , router = express.Router();

var credentials = require('../config/credentials')
    , db = require('../db')
    , firebaseMessaging = require('../lib/firebase-messaging')(credentials.fcm_server_key);

router.post('/users/:username/deviceToken', function (req, res) {
    if (!req.body.deviceToken) {
        res.status(500).send('deviceToken is required');
    } else {
        console.log(`Update deviceToken for "${req.params.username}" with token "${req.body.deviceToken}"`)
        db.put(req.params.username, req.body.deviceToken, function (err) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.send("OK");
            }
        });
    }
});

router.get('/users', function (req, res) {
    users = [];

    db.createReadStream()
        .on('data', function (v) { users.push(v.key) })
        .on('close', function () { res.send(users) });
})

router.post('/users/:username/messages', function (req, res) {
    db.get(req.params.username, function (err, deviceToken) {
        if (err) {
            console.error("The token for %s is not found!", req.params.username)
            res.status(500).send(err);
        } else {
            console.info(`${req.body.fromUser} said "${req.body.content}" to ${req.params.username}`)
            firebaseMessaging.sendMessage({
                notification: {
                    title: `${req.body.fromUser} said`,
                    text: req.body.content,
                },
                data: {
                    fromUser: req.body.fromUser,
                    content: req.body.content,
                },
                to: deviceToken
            }, (err, r) => {
                if (err) {
                    res.status(500).send(err)
                } else {
                    res.send(r)
                }
            });
        }
    });
});

module.exports = router;