FROM node

MAINTAINER Xiaoxun Zeng(xiaoxun.zeng@fleetcor.com)

ENV APP_ROOT=/firebase-prototype-server

COPY . $APP_ROOT

VOLUME $APP_ROOT/db

WORKDIR $APP_ROOT

RUN npm install --only=production

CMD npm start

EXPOSE 3000