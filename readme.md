Firebase Prototype Server
A sample server for firebase cloud messaging. 

## Build
    docker build . -t firebase-prototype-server
## Run
    docker run -p 3000:3000 -v /tmp/db:/firebase-prototype-server/db firebase-prototype-server
## Available APIs
Endpoint                                  | Description
------------------------------------------|-------------------------------------
**GET** /api/v1/users                     | Get all existed users.
**POST** /api/v1/users/{user}/deviceToken | Change the device token for the user.
**POST** /api/v1/users/{user}/messages    | Send a message to user.

## See Also
1. https://firebase.google.com/docs/cloud-messaging/http-server-ref
2. https://developers.google.com/instance-id/reference/server