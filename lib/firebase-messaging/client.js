const https = require('https')

class Client {
    constructor(serverKey) {
        this.serverKey = serverKey
    }

    sendMessage(message, callback) {
        var postData = JSON.stringify(message);
        var options = {
            hostname: 'fcm.googleapis.com',
            path: '/fcm/send',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(postData),
                'Authorization': `key=${this.serverKey}`
            }
        }

        var result = "";
        var req = https.request(options, (res) => {
            res.setEncoding('utf8');
            res.on('data', (chunk) => {result += chunk});
            res.on('end', () => {callback(null, JSON.parse(result))})
        });

        req.on('error', (err) => {
            if (err) {
                callback(err)
            }
        });

        req.write(postData);
        req.end();
    }
}

module.exports = function(serverKey) {
    return new Client(serverKey);
}