var express = require('express')
    , bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

app.use(require('./routes'));

module.exports = app;

if (require.main === module) {
    var server = app.listen(3000, function () {
        require('dns').lookup(require('os').hostname(), function (err, add, fam) {
            console.log('FirebasePrototype server listening at http://%s:%s', add, server.address().port);
        })
    });
}